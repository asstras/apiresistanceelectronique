
require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const connexion = require('./connectionBdd')
const resistanceRoute = require('./src/routes/Resistance_Routes')
const userRoute = require('./src/routes/User_Route')
const categorieRoute = require('./src/routes/Categorie_Routes')

connexion

const app = express()

app.use(cors())

app.use(bodyParser.json())

app.use('/resistance', resistanceRoute)
app.use('/user', userRoute)
app.use('/categorie', categorieRoute)

app.get('/', (req, res, next) => {
    res.status(200).json({message : 'Mes résistances sont à -50%'})
})

module.exports = app
