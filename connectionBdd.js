
const mongoose = require('mongoose');
require('dotenv').config();

const connexion = mongoose.connect(process.env.URL_MONGOOSE, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => { console.log('connexion ok'); }).catch(err => { console.log('erreur de co ' + err); })
module.exports = connexion;