// sa requis l'application avec son chemin
const app = require('./application');

//sa requiet avec ces paramètres .config
require('dotenv').config();

//on initialise la variable port qui est égale à PORT_ENV
const port = process.env.PORT_ENV;

//appl qui va écouter le port si il a réussi il retourne un message + la valeur de la variable port
app.listen(port, () => {
    console.log("tourne sur le port " + port)
})