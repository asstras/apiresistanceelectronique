const Categories = require('../models/Categories').Categories;

exports.newCategorie = (req, res) => {
    const newCategorie = new Categories({
        name: req.body.name
    })

    newCategorie.save().then(categorie => {
        res.status(200).json({
            message: "La catégorie à bien étais créer",
            categorie: categorie
        });
    }).catch(err => {
        res.status(500).json({
            message: "une erreur sur à la création de la catégorie",
            err: err
        })
    })
}

exports.getAllCategories = (req, res) => {
    Categories.find().then(categories => res.status(200).json({ categories: categories })).catch(err => res.status(404).json({ err: err }));
}

exports.deleteCategories = (req, res) => {

    Categories.findByIdAndDelete(req.params.id)
        .then(() => res.status(201).json({ message: "La catégorie est supprimé" }))
        .catch(err => res.status(500).json({ err: err }))
}

exports.updateCategories = (req, res) => {
    Categories.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        type: req.body.type
    })

        .then(message_categorie => {
            res.status(200).json({
                message: "La catégorie à bien étais modifié",
                categorie: message_categorie
            });
        }).catch(err => {
            res.status(500).json({
                message: "une erreur sur la modification de la catégorie",
                err: err
            })
        })
}