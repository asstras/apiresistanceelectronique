const Resistances = require('../models/Resistances').Resistances;

exports.newResistance = (req, res) =>
{
    const newResistance = new Resistances({
        name : req.body.name,
        type : req.body.type,
        id_categorie: req.body.id_categorie
    })

    newResistance.save().then(resistance => 
        {
            res.status(200).json({
                message : "L'article à bien étais créer",
                resistance : resistance
            });
        }).catch(err => {
            res.status(500).json({
                message : "une erreur sur à la création de l'article",
                err: err
            })
        })
}

exports.getAllResistances  = (req, res) => {
    Resistances.find().then(resistances => res.status(200).json({ resistances : resistances})).catch(err => res.status(404).json({ err: err}));
}

exports.deleteResistances = (req, res) => {
    
    Resistances.findByIdAndDelete(req.params.id)
    .then(() => res.status(201).json({message : "L'article est supprimé"}))
    .catch(err => res.status(500).json({ err : err }))
}

exports.updateResistances = (req, res) => {
    Resistances.findByIdAndUpdate(req.params.id,{
        name: req.body.name,
        type: req.body.type
    })

    .then(message_resistance => {
        res.status(200).json({
            message: "L'article à bien étais modifié",
            resistance: message_resistance
        });
    }).catch(err => {
        res.status(500).json({
            message: "une erreur sur à la création de l'article",
            err: err
        })
    })
}

exports.getOneResistance = (req, res) => {
    Resistances.findById(req.params.id).then(resistances => res.status(200).json({ resistances : resistances})).catch(err => res.status(404).json({ err:err}));
}