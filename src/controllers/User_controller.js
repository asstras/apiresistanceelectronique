
const User = require('../models/User').User;
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
require('dotenv').config();


/**
 * 
 * on stock dans nos constantes les attributs l'email, name, password au sein du body de notre requete post
 */
exports.signUp = (req, res) => {
    console.log(req);
    const name = req.body.name;
    const email = req.body.email;
    const password = req.body.password;
    const role = req.body.role;
    // on utilse la methode find pour verifier si l'email existe deja en filtre
    User.find({ email: email }).then(user => {
        if (user.length > 0) {
            //  dans le cas ou la promesse est positive, on verifie si le tableau retourné est > à 0
            // on envoit un msg indiquant qu'un email est deja existant
            res.status(503).json({ message: "L'email est déja existant" });
        }
        // sinon on va utiliser le librairie bcryptjs qui genere de facon asynchrone(en parallelle) un hash à partir du password
        // et on precise la longueur du salage (cryptage de 12caracteres) (modifie le caractere existant et en rajouter, après ca va le hashé (salt))
        else {
            bcrypt.hash(password, 12).then(hashedPassword => {
                // en retour de promesse positive
                // on instencie un class user avec les attributs precedents ainsi que le mdp hashé
                const newUser = new User({
                    email: email,
                    name: name,
                    password: hashedPassword,
                    role : role
                })
                //   puis on sauvegarde en bdd cette nouvelle instance 
                newUser.save().then(user => {
                    res.status(200).json({ user: user })
                }).catch(err => res.status(500).json({ message: err }));
            })
        }
    })
}
/**
  * on stock dans nos constantes les attributs: email, password du body de notre requête en post
  * on utlise la méthode findOne qui récupère un objet dans la bdd à partir de l'email rentré
  * dans le retour de la promesse positive on utilise la méthode compare du module bcrypt qui consiste à comparé de manière asynchrone
  * notre password en claire au password hashé de la bdd en cas de retour positif si la boolean est true alors on va créer un token grâce à la
  * fonction sign de jsonwebtoken qui prend en premier paramètre un objet contenant les attributs email, userId en deuxième paramètre
  * on pointe vers la variable environnement secret token qui va servir de signature puis troisième paramètre on précise le délai
  * d'expiration qui est de 1h et on renvoi un objet contenant le token créer et le userEmail sinon en cas de boolean false on renvoie un message précisant
  * que le mot de passe est incorrecte en cas de retour négatif de la promesse de compare on renvoie un message d'erreur
  * en cas de retour négative de findOne on renvoie un message qui indique que l'email n'existe pas. 
 */
exports.signIn = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({ email: email }).then(user => {
        bcrypt.compare(password, user.password).then(isEqual => {
            if (isEqual) {
                const token = jwt.sign({
                    email: user.email,
                    userId: user._id.toString()
                },
                    process.env.JWT_SECRET_TOKEN,
                    {
                        expiresIn: '1h'
                    });

                res.status(200).json({ token: token, userEmail: user.email })
            } else {
                res.status(503).json({ message: "Le mot de passe est incorrecte" })
            }

        }).catch(err => res.status(400).json({ message: "Une erreur c'est produite", err: err }));
    }).catch(err => {
        res.status(503).json({ message: "L'email n'existe pas" });
    })
}