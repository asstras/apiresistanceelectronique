const mongoose = require('mongoose');

const CategorieSchema = new mongoose.Schema({
    name: { type: String, required: true }
})

module.exports.Categories = mongoose.model('Categories', CategorieSchema);