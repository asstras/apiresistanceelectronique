const mongoose = require('mongoose');

const ResistanceSchema = new mongoose.Schema({
    name : {type : String, required: true},
    type : {type : String, required: true},
    id_categorie: {type :String, required:true}
})

module.exports.Resistances = mongoose.model('Resistances', ResistanceSchema);