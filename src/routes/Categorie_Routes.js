const router = require('express').Router();
const categorieController = require('../controllers/Categorie_Controller');

router.post('/newCategorie', categorieController.newCategorie);

router.get('/allCategories', categorieController.getAllCategories);

router.delete('/deleteCategorie/:id', categorieController.deleteCategories)

router.post('/updateCategorie/:id', categorieController.updateCategories)

module.exports = router