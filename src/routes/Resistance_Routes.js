const router = require('express').Router();
const resistanceController = require('../controllers/Resistance_Controller');

router.post('/newResistance', resistanceController.newResistance);

router.get('/allResistances', resistanceController.getAllResistances);

router.delete('/deleteResistance/:id', resistanceController.deleteResistances)

router.post('/updateResistance/:id', resistanceController.updateResistances)

router.get('/getOneResistance/:id', resistanceController.getOneResistance)

module.exports =router