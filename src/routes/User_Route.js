const router = require('express').Router()
const userController = require('../controllers/User_controller')
const validateToken = require('../providers/validateToken')


router.post('/signUp', userController.signUp)
router.post('/signIn', userController.signIn)
router.post('/validateToken', validateToken.validateToken)

module.exports = router;